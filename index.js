"use strict";

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _slicedToArray2 = require("babel-runtime/helpers/slicedToArray");

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _toConsumableArray2 = require("babel-runtime/helpers/toConsumableArray");

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var visit = require("unist-util-visit");
var isRelativeUrl = require("is-relative-url");
var fs = require("fs");
var fsExtra = require("fs-extra");
var path = require("path");
var _ = require("lodash");
var cheerio = require("cheerio");
var imageSize = require("probe-image-size");

var DEPLOY_DIR = "public";

var invalidDestinationDirMessage = function invalidDestinationDirMessage(dir) {
  return "[gatsby-remark-copy-linked-files You have supplied an invalid destination directory. The destination directory must be a child but was: " + dir;
};

// dir must be a child
var destinationDirIsValid = function destinationDirIsValid(dir) {
  return !path.relative("./", dir).startsWith("..");
};

var validateDestinationDir = function validateDestinationDir(dir) {
  return !dir || dir && destinationDirIsValid(dir);
};

var newFileName = function newFileName(linkNode) {
  return linkNode.name + "-" + linkNode.internal.contentDigest + "." + linkNode.extension;
};

var newPath = function newPath(linkNode, destinationDir) {
  if (destinationDir) {
    return path.posix.join(process.cwd(), DEPLOY_DIR, destinationDir, newFileName(linkNode));
  }
  return path.posix.join(process.cwd(), DEPLOY_DIR, newFileName(linkNode));
};

var newLinkURL = function newLinkURL(linkNode, destinationDir, pathPrefix) {
  var _path$posix;

  var linkPaths = ["/", pathPrefix, destinationDir, newFileName(linkNode)].filter(function (lpath) {
    if (lpath) return true;
    return false;
  });

  return (_path$posix = path.posix).join.apply(_path$posix, (0, _toConsumableArray3.default)(linkPaths));
};

function toArray(buf) {
  var arr = new Array(buf.length);

  for (var i = 0; i < buf.length; i++) {
    arr[i] = buf[i];
  }

  return arr;
}

module.exports = function (_ref) {
  var files = _ref.files,
      markdownNode = _ref.markdownNode,
      markdownAST = _ref.markdownAST,
      pathPrefix = _ref.pathPrefix,
      getNode = _ref.getNode;
  var pluginOptions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var defaults = {
    ignoreFileExtensions: ["png", "jpg", "jpeg", "bmp", "tiff"]
  };
  var destinationDir = pluginOptions.destinationDir;

  if (!validateDestinationDir(destinationDir)) return Promise.reject(invalidDestinationDirMessage(destinationDir));

  var options = _.defaults(pluginOptions, defaults);

  var filesToCopy = new Map();
  // Copy linked files to the destination directory and modify the AST to point
  // to new location of the files.
  var visitor = function visitor(link) {
    if (isRelativeUrl(link.url) && getNode(markdownNode.parent).internal.type === "File") {
      var linkPath = path.posix.join(getNode(markdownNode.parent).dir, link.url);
      var linkNode = _.find(files, function (file) {
        if (file && file.absolutePath) {
          return file.absolutePath === linkPath;
        }
        return null;
      });
      if (linkNode && linkNode.absolutePath) {
        var newFilePath = newPath(linkNode, options.destinationDir);

        // Prevent uneeded copying
        if (linkPath === newFilePath) return;

        var linkURL = newLinkURL(linkNode, options.destinationDir, pathPrefix);
        link.url = linkURL;
        filesToCopy.set(linkPath, newFilePath);
      }
    }
  };

  // Takes a node and generates the needed images and then returns
  // the needed HTML replacement for the image
  var generateImagesAndUpdateNode = function generateImagesAndUpdateNode(image, node) {
    var imagePath = path.posix.join(getNode(markdownNode.parent).dir, image.attr("src"));
    var imageNode = _.find(files, function (file) {
      if (file && file.absolutePath) {
        return file.absolutePath === imagePath;
      }
      return null;
    });
    if (!imageNode || !imageNode.absolutePath) {
      return;
    }

    var initialImageSrc = image.attr("src");
    // The link object will be modified to the new location so we'll
    // use that data to update our ref
    var link = { url: image.attr("src") };
    visitor(link);
    node.value = node.value.replace(new RegExp(image.attr("src"), "g"), link.url);

    var dimensions = void 0;

    if (!image.attr("width") || !image.attr("height")) {
      dimensions = imageSize.sync(toArray(fs.readFileSync(imageNode.absolutePath)));
    }

    // Generate default alt tag
    var srcSplit = initialImageSrc.split("/");
    var fileName = srcSplit[srcSplit.length - 1];
    var fileNameNoExt = fileName.replace(/\.[^/.]+$/, "");
    var defaultAlt = fileNameNoExt.replace(/[^A-Z0-9]/gi, " ");

    image.attr("alt", image.attr("alt") ? image.attr("alt") : defaultAlt);
    image.attr("width", image.attr("width") ? image.attr("width") : dimensions.width);
    image.attr("height", image.attr("height") ? image.attr("height") : dimensions.height);
  };

  visit(markdownAST, "link", function (link) {
    var ext = link.url.split(".").pop();
    if (options.ignoreFileExtensions.includes(ext)) {
      return;
    }

    visitor(link);
  });

  // This will only work for markdown img tags
  visit(markdownAST, "image", function (image) {
    var ext = image.url.split(".").pop();
    if (options.ignoreFileExtensions.includes(ext)) {
      return;
    }

    // since dir will be undefined on non-files
    if (markdownNode.parent && getNode(markdownNode.parent).internal.type !== "File") {
      return;
    }

    var imagePath = path.posix.join(getNode(markdownNode.parent).dir, image.url);
    var imageNode = _.find(files, function (file) {
      if (file && file.absolutePath) {
        return file.absolutePath === imagePath;
      }
      return false;
    });

    if (imageNode) {
      visitor(image);
    }
  });

  // For each HTML Node
  visit(markdownAST, "html", function (node) {
    var $ = cheerio.load(node.value);

    // Handle Images
    var imageRefs = [];
    $("img").each(function () {
      try {
        if (isRelativeUrl($(this).attr("src"))) {
          imageRefs.push($(this));
        }
      } catch (err) {
        // Ignore
      }
    });

    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = imageRefs[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var thisImg = _step.value;

        try {
          var ext = thisImg.attr("src").split(".").pop();
          if (options.ignoreFileExtensions.includes(ext)) {
            return;
          }

          generateImagesAndUpdateNode(thisImg, node);
        } catch (err) {
          // Ignore
        }
      }

      // Handle video tags.
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }

    var videoRefs = [];
    $("video source").each(function () {
      try {
        if (isRelativeUrl($(this).attr("src"))) {
          videoRefs.push($(this));
        }
      } catch (err) {
        // Ignore
      }
    });

    var _iteratorNormalCompletion2 = true;
    var _didIteratorError2 = false;
    var _iteratorError2 = undefined;

    try {
      for (var _iterator2 = videoRefs[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
        var thisVideo = _step2.value;

        try {
          var _ext = thisVideo.attr("src").split(".").pop();
          if (options.ignoreFileExtensions.includes(_ext)) {
            return;
          }

          // The link object will be modified to the new location so we'll
          // use that data to update our ref
          var link = { url: thisVideo.attr("src") };
          visitor(link);
          node.value = node.value.replace(new RegExp(thisVideo.attr("src"), "g"), link.url);
        } catch (err) {
          // Ignore
        }
      }

      // Handle audio tags.
    } catch (err) {
      _didIteratorError2 = true;
      _iteratorError2 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion2 && _iterator2.return) {
          _iterator2.return();
        }
      } finally {
        if (_didIteratorError2) {
          throw _iteratorError2;
        }
      }
    }

    var audioRefs = [];
    $("audio source").each(function () {
      try {
        if (isRelativeUrl($(this).attr("src"))) {
          audioRefs.push($(this));
        }
      } catch (err) {
        // Ignore
      }
    });

    var _iteratorNormalCompletion3 = true;
    var _didIteratorError3 = false;
    var _iteratorError3 = undefined;

    try {
      for (var _iterator3 = audioRefs[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
        var thisAudio = _step3.value;

        try {
          var _ext2 = thisAudio.attr("src").split(".").pop();
          if (options.ignoreFileExtensions.includes(_ext2)) {
            return;
          }

          var _link = { url: thisAudio.attr("src") };
          visitor(_link);
          node.value = node.value.replace(new RegExp(thisAudio.attr("src"), "g"), _link.url);
        } catch (err) {
          // Ignore
        }
      }

      // Handle a tags.
    } catch (err) {
      _didIteratorError3 = true;
      _iteratorError3 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion3 && _iterator3.return) {
          _iterator3.return();
        }
      } finally {
        if (_didIteratorError3) {
          throw _iteratorError3;
        }
      }
    }

    var aRefs = [];
    $("a").each(function () {
      try {
        if (isRelativeUrl($(this).attr("href"))) {
          aRefs.push($(this));
        }
      } catch (err) {
        // Ignore
      }
    });

    var _iteratorNormalCompletion4 = true;
    var _didIteratorError4 = false;
    var _iteratorError4 = undefined;

    try {
      for (var _iterator4 = aRefs[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
        var thisATag = _step4.value;

        try {
          var _ext3 = thisATag.attr("href").split(".").pop();
          if (options.ignoreFileExtensions.includes(_ext3)) {
            return;
          }

          var _link2 = { url: thisATag.attr("href") };
          visitor(_link2);

          node.value = node.value.replace(new RegExp(thisATag.attr("href"), "g"), _link2.url);
        } catch (err) {
          // Ignore
        }
      }
    } catch (err) {
      _didIteratorError4 = true;
      _iteratorError4 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion4 && _iterator4.return) {
          _iterator4.return();
        }
      } finally {
        if (_didIteratorError4) {
          throw _iteratorError4;
        }
      }
    }

    return;
  });

  return Promise.all(Array.from(filesToCopy, function () {
    var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
      var _ref4 = (0, _slicedToArray3.default)(_ref2, 2),
          linkPath = _ref4[0],
          newFilePath = _ref4[1];

      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (fsExtra.existsSync(newFilePath)) {
                _context.next = 11;
                break;
              }

              _context.prev = 1;
              _context.next = 4;
              return fsExtra.ensureDir(path.dirname(newFilePath));

            case 4:
              _context.next = 6;
              return fsExtra.copy(linkPath, newFilePath);

            case 6:
              _context.next = 11;
              break;

            case 8:
              _context.prev = 8;
              _context.t0 = _context["catch"](1);

              console.error("error copying file", _context.t0);

            case 11:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, undefined, [[1, 8]]);
    }));

    return function (_x2) {
      return _ref3.apply(this, arguments);
    };
  }()));
};